import 'dart:io';

void main(List<String> arguments) {
  String? string = stdin.readLineSync();
  List<String>? list = toList(string!);
  List<String> postfix = toPostfix(list);

  print(EvaluatePostfix(postfix));
}

List<String> toPostfix(List<String> list) {
  List<String> operators = [];
  List<String> postfix = [];
  list.forEach((tk) {
    if (int.tryParse(tk) != null) {
      postfix.add(tk);
    }
    if (isOperator(tk)) {
      while (!operators.isEmpty &&
          operators.last != "(" &&
          precedence(tk) <= precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(tk);
    }
    if (tk == "(") {
      operators.add(tk);
    }
    if (tk == ")") {
      while (operators.last != "(") {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  });

  while (!operators.isEmpty) {
    postfix.add(operators.removeLast());
  }

  return postfix;
}

List<String> toList(String input) {
  List<String>? list = input.split(" ");
  return list;
}

bool isOperator(String tk) {
  switch (tk) {
    case "+":
      return true;
    case "-":
      return true;
    case "*":
      return true;
    case "/":
      return true;
  }
  return false;
}

int precedence(String tk) {
  switch (tk) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
      return 2;
    case "(":
      return 3;
  }
  return -1;
}

double EvaluatePostfix(List<String> postfix) {
  List<double> values = [];
  postfix.forEach((tk) {
    if (int.tryParse(tk) != null) {
      values.add(double.parse(tk));
    } else {
      double x = values.removeLast();
      double y = values.removeLast();
      switch (tk) {
        case "+":
          values.add(y + x);
          break;
        case "-":
          values.add(y - x);
          break;
        case "*":
          values.add(y * x);
          break;
        case "/":
          values.add(y / x);
          break;
        default:
          break;
      }
    }
  });

  return values.first;
}
